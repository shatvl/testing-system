<?php
/**
 * Created by PhpStorm.
 * User: Vladislav
 * Date: 20.03.2017
 * Time: 21:18
 */

namespace AppBundle\Entity;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * Class Logs
 * @package AppBundle\Entity
 *
 * @ORM\Entity
 * @ORM\Table(name="log")
 */
class Logs
{
    const USER_TEST_COMPLETED = 2001;
    const USER_TEST_STARTED = 2002;
    const USER_LOGIN = 1001;
    const USER_LOGOUT = 1002;

    /**
     * @var integer $id
     *
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string message
     * @ORM\Column(type="text")
     */
    private $message;

    /**
     * @var string message
     * @ORM\Column(type="integer")
     */
    private $code;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * @param string $message
     */
    public function setMessage($message)
    {
        $this->message = $message;
    }

    /**
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @param string $code
     */
    public function setCode($code)
    {
        $this->code = $code;
    }


}