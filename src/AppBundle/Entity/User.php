<?php
/**
 * Created by PhpStorm.
 * User: Vladislav
 * Date: 03.11.2016
 * Time: 13:22
 */

namespace AppBundle\Entity;

use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="fos_user")
 */
class User extends BaseUser
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * Many users have many tests
     * @ORM\OneToMany(targetEntity="UsersTests", mappedBy="user")
     */
    private $usersTests;

    public function __construct()
    {
        parent::__construct();

        $this->tests = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * @return \Doctrine\Common\Collections\ArrayCollection
     */
    public function getTests()
    {
        return $this->tests;
    }

    /**
     * @param Test $test
     * @return User|bool
     */
    public function addTest(Test $test)
    {
        if($this->tests->contains($test)) {
            return false;
        }

        $this->tests->add($test);

        return $this;
    }
}