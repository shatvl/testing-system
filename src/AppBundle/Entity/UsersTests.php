<?php
/**
 * Created by PhpStorm.
 * User: Vladislav
 * Date: 19.03.2017
 * Time: 15:30
 */

namespace AppBundle\Entity;

use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class UsersTests
 * @package AppBundle\Entity
 *
 * @ORM\Entity
 * @ORM\Table(name="users_tests")
 */
class UsersTests
{
    /**
     * @var integer id
     *
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \DateTime $updatedAt
     *
     * @ORM\Column(type="datetime", name="updated_at")
     */
    private $updatedAt;

    /**
     * @var integer $correctAnswersCnt
     *
     * @ORM\Column(type="integer", name="correct_answers_cnt")
     */
    private $correctAnswersCnt;

    /**
     * @var integer $incorrectAnswersCnt
     *
     * @ORM\Column(type="integer", name="incorrect_answers_cnt")
     */
    private $uncorrectAnswersCnt;

    /**
     * @var float $mark;
     *
     * @ORM\Column(type="float")
     */
    private $mark;

    /**
     * @var \DateTime $createdAt
     *
     * @ORM\Column(type="datetime", name="created_at")
     */
    private $createdAt;

    /**
     * @ORM\ManyToOne(targetEntity="Test")
     * @ORM\JoinColumn(name="test_id", referencedColumnName="id", nullable=false)
     */
    private $test;

    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="usersTests")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", nullable=false)
     */
    private $user;

    /**
     * @return float
     */
    public function getMark()
    {
        return $this->mark;
    }

    /**
     * @param float $mark
     * @return $this
     */
    public function setMark($mark)
    {
        $this->mark = $mark;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param \DateTime $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @param \DateTime $updatedAt
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
    }

    /**
     * @return int
     */
    public function getCorrectAnswersCnt()
    {
        return $this->correctAnswersCnt;
    }

    /**
     * @param int $correctAnswersCnt
     */
    public function setCorrectAnswersCnt($correctAnswersCnt)
    {
        $this->correctAnswersCnt = $correctAnswersCnt;
    }

    /**
     * @return int
     */
    public function getUncorrectAnswersCnt()
    {
        return $this->uncorrectAnswersCnt;
    }

    /**
     * @param int $uncorrectAnswersCnt
     */
    public function setUncorrectAnswersCnt($uncorrectAnswersCnt)
    {
        $this->uncorrectAnswersCnt = $uncorrectAnswersCnt;
    }

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @return mixed
     */
    public function getTest()
    {
        return $this->test;
    }

    /**
     * @param mixed $test
     */
    public function setTest($test)
    {
        $this->test = $test;
    }

    /**
     * @param mixed $user
     */
    public function setUser($user)
    {
        $this->user = $user;
    }
}