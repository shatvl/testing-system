<?php

namespace AppBundle\Repository;

use AppBundle\Entity\Test;
use AppBundle\Entity\User;
use AppBundle\Entity\UsersTests;

/**
 * Created by PhpStorm.
 * User: Vladislav
 * Date: 20.03.2017
 * Time: 19:15
 */
class TestsRepository extends \Doctrine\ORM\EntityRepository
{
    /**
     * Get all Tests (uncompleted and completed with marks)
     *
     * @param User $user
     * @return array
     */
    public function getAllUncompletedTestsForUser(User $user)
    {
        $dql = "SELECT test FROM AppBundle:Test as test WHERE test.id NOT IN (SELECT IDENTITY(ut.test) FROM AppBundle:UsersTests as ut WHERE ut.user = :userId)";

        $uncompletedTests = $this->getEntityManager()->createQuery($dql)->setParameter('userId', $user->getId())->getResult();

        $dql = "SELECT test FROM AppBundle:Test as test WHERE test.id IN (SELECT IDENTITY(ut.test) FROM AppBundle:UsersTests as ut WHERE ut.user = :userId)";

        $completedTests = $this->getEntityManager()->createQuery($dql)->setParameter('userId', $user->getId())->getResult();
        $ct = [];
        /** @var Test $completedTest */
        foreach ($completedTests as $key => $completedTest) {
            /** @var UsersTests $userTest */
            $userTest = $this->getEntityManager()->getRepository('AppBundle:UsersTests')->findOneBy(['test' => $completedTest]);
            $ct[$key]['mark'] = $userTest->getMark();
            $ct[$key]['test'] = $completedTest;
        }

        return array(
            'completedTests' => $ct,
            'uncompletedTests' => $uncompletedTests
        );
    }

    /**
     * Get All Tests
     *
     * @return array
     */
    public function getAllTests()
    {
        $tests = $this->findAll();

        return array(
            'completedTests' => [],
            'uncompletedTests' => $tests
        );
    }
}