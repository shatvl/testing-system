<?php
/**
 * Created by PhpStorm.
 * User: Vladislav
 * Date: 04.11.2016
 * Time: 14:02
 */

namespace AppBundle\Form;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Vich\UploaderBundle\Form\Type\VichFileType;

class CreateNewTestType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     * @return void
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('name', TextType::class, array(
            'required' => true,
            'label' => false,
            'attr' => array(
                'class' => 'form-control',
                'placeholder' => 'Test name')
        ));
        $builder->add('imageFile', VichFileType::class, array(
            'required' => false,
            'label' => false
        ));
        $builder->add('save', SubmitType::class, array(
            'attr' => array(
                'class' => 'btn btn-success',
            ),
        ));
    }

    /**
     * Gets the unique name of this form.
     * @return string
     */
    public function getName()
    {
        return 'new_test';
    }
}