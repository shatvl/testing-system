<?php
/**
 * Created by PhpStorm.
 * User: Vladislav
 * Date: 07.11.2016
 * Time: 22:01
 */

namespace AppBundle\Form;


use Doctrine\Common\Collections\Collection;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\DomCrawler\Form;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Vich\UploaderBundle\Form\Type\VichFileType;

class CreateNewQuestionType extends AbstractType
{
    /**
     * @param  FormBuilderInterface $builder
     * @param array $options
     * @return void
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('questionText', TextareaType::class, array(
            'required' => false,
        ));
        $builder->add('imageFile', VichFileType::class, array(
            'required' => false,
            'label' => false
        ));
        $builder->add('save', SubmitType::class, array(
            'attr' => array(
                'class' => 'btn btn-success',
            ),
        ));
        $builder->add('answers', CollectionType::class, array(
            'label' => false,
            'by_reference' => false,
            'entry_type' => CreateNewAnswerType::class,
            'required' => false,
            'allow_add' => true,
            'allow_delete' => true
        ));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Question',
        ));
    }

    /**
     * Gets the unique name of this form.
     * @return string
     */
    public function getName()
    {
        return 'new_question';
    }
}