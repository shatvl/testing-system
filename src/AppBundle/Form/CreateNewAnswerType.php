<?php
/**
 * Created by PhpStorm.
 * User: Vladislav
 * Date: 08.11.2016
 * Time: 1:18
 */

namespace AppBundle\Form;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Vich\UploaderBundle\Form\Type\VichFileType;

class CreateNewAnswerType extends AbstractType
{
    /**
     * @param  FormBuilderInterface $builder
     * @param array $options
     * @return void
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('text', TextareaType::class, array(
            'required' => false,
        ));
        $builder->add('imageFile', VichFileType::class, array(
            'required' => false,
            'label' => false
        ));
        $builder->add('correct', CheckboxType::class, array(
            'label' => 'Is it correct answer?',
            'required' => false,
         ));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Answer',
        ));
    }

    /**
     * Gets the unique name of this form.
     * @return string
     */
    public function getName()
    {
        return 'new_question';
    }
}