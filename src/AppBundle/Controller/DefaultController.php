<?php

namespace AppBundle\Controller;

use AppBundle\AppBundle;
use AppBundle\Entity\Answer;
use AppBundle\Entity\Question;
use AppBundle\Entity\Test;
use AppBundle\Entity\UsersTests;
use AppBundle\Repository\TestsRepository;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Tools\Console\Helper\EntityManagerHelper;
use DOMDocument;
use Liip\ImagineBundle\Imagine\Cache\CacheManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Response;
use Vich\UploaderBundle\Templating\Helper\UploaderHelper;
use \CloudConvert\Api;

/**
 * @property  container
 */
class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @Template()
     *
     */
    public function indexAction(Request $request)
    {
        $entityManager = $this->getDoctrine()->getManager();
        /** @var TestsRepository $testRepository */
        $testsRepository = $entityManager->getRepository(Test::class);

        if ($user = $this->getUser()) {
            $tests = $testsRepository->getAllUncompletedTestsForUser($this->getUser());
        } else {
            $tests = $testsRepository->getAllTests();
        }

        return $this->render('AppBundle::index.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.root_dir').'/..').DIRECTORY_SEPARATOR,
            'uncompletedTests' => $tests['uncompletedTests'],
            'completedTests' => $tests['completedTests']
        ]);
    }

    /**
     * @Route("/test-result/{id}", name="testResult")
     * @param Request $request
     * @return Response
     */
    public function testResult(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $test = $em->getRepository(Test::class)->find($id);

        if (!$test) {
            return new Response("false");
        }

        $correctCnt = $request->request->get('correct');
        $incorrectCnt = $request->request->get('all');

        $mark = round($correctCnt / $incorrectCnt, 1) * 10;

        $usersTests = new UsersTests();
        $usersTests->setMark($mark);
        $usersTests->setCorrectAnswersCnt($correctCnt);
        $usersTests->setUncorrectAnswersCnt($incorrectCnt - $correctCnt);
        $usersTests->setUser($this->getUser());
        $usersTests->setTest($test);
        $usersTests->setCreatedAt(new \DateTime());
        $usersTests->setUpdatedAt(new \DateTime());

        $em->persist($usersTests);
        $em->flush();

        return new Response("true");
    }

    /**
     * @Route("/test/{id}", name="startTest")
     * @param Request $request
     * @param $id
     * @return Response
     */
    public function startTest(Request $request, $id)
    {
        $entityManager = $this->getDoctrine()->getManager();
        /** @var Test $test */
        $test = $entityManager->getRepository(Test::class)->find($id);
        /** @var Collection $questions */
        $questions = $test->getQuestions();
        $result = [];
        /** @var UploaderHelper $uploaderHelper */
        $uploaderHelper = $this->container->get('vich_uploader.templating.helper.uploader_helper');
        /** @var CacheManager $imagineCacheManager */
        $imagineCacheManager = $this->get('liip_imagine.cache.manager');
        /** @var Question $question */
        foreach ($questions as $question) {
            $temp = [];
            $temp['q'] = $question->getQuestionText();
            if ($question->getImageName()) {
                $path = $uploaderHelper->asset($question, 'imageFile');
                $path = $imagineCacheManager->getBrowserPath($path, 'my_thumb');
                $temp['q'] .= " <img src='$path'/>";
            }
            $answers = $question->getAnswers();
            /** @var Answer $answer */
            $options = [];
            $answAmount = count($answers);
            for ($i = 0; $i < $answAmount; $i++) {
                $answerText = $answers[$i]->getText();
                if ($answers[$i]->getImageName()) {
                    $path = $uploaderHelper->asset($answers[$i], 'imageFile');
                    $path = $imagineCacheManager->getBrowserPath($path, 'my_thumb');
                    $answerText .= " <img src='$path'/>";
                }
                $options[] = $answerText;
                if ($answers[$i]->isCorrect()) {
                    $temp['correctIndex'] = $i;
                }
            }
            $temp['options'] = $options;
            $result[] = $temp;
        }

        return new Response(json_encode($result));
    }
}
