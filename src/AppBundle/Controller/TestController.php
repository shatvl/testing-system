<?php
/**
 * Created by PhpStorm.
 * User: Vladislav
 * Date: 04.11.2016
 * Time: 13:11
 */

namespace AppBundle\Controller;

use AppBundle\Entity\Question;
use AppBundle\Entity\Test;
use AppBundle\Form\CreateNewQuestionType;
use AppBundle\Form\CreateNewTestType;
use Doctrine\ORM\EntityManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Response;

class TestController extends Controller
{
    /**
     * @Route("/get_all_tests", name="getAllTests")
     * @param Request $request
     * @return Response
     */
    public function getAllTestsAction(Request $request)
    {
        $entityManager = $this->getDoctrine()->getEntityManager();
        $tests = $entityManager->getRepository(Test::class)->findBy([], ['id' => 'desc']);

        return $this->render('AppBundle:admin:test/tests.html.twig', ['tests' => $tests]);
    }

    public function getAllCompletedTests(Request $request)
    {
        $entityManager = $this->getDoctrine()->getManager();

    }

    /**
     * @Route("/create_new_test", name="new_test")
     * @param Request $request
     * @return Response
     * @Template()
     */
    public function newTestAction(Request $request)
    {
        $action = $request->request->get('action');
        /** @var EntityManager $entityManager */
        $entityManager = $this->getDoctrine()->getManager();
        if($action == 'new' || !$action) {
            $test = new Test();
            $newTestForm = $this->createForm(CreateNewTestType::class, $test);
            $newTestForm->handleRequest($request);

            if ($newTestForm->isValid()) {
                $entityManager->persist($test);
                $entityManager->flush();
            }

            return $this->render('AppBundle:admin:test/new_test.html.twig', ['form' => $newTestForm->createView()]);

        } elseif ($action == 'delete') {
            $test = $entityManager->getRepository(Test::class)->find($request->request->get('testID'));
            if ($test) {
               $entityManager->remove($test);
               $entityManager->flush();

               return new Response("true");
            } else {
               return new Response("false");
            }
        } elseif ($action == 'edit') {
            $test = $entityManager->getRepository(Test::class)->find($request->request->get('testID'));
            $editTestForm = $this->createForm(CreateNewTestType::class, $test);
            $editTestForm->handleRequest($request);

            if($editTestForm->isValid()) {
                $entityManager->persist($test);
                $entityManager->flush();
            }

            return $this->render('AppBundle:admin:test/new_test.html.twig', ['form' => $editTestForm->createView()]);
        }

        return new Response("");
    }

    /**
     * @Route("/create_new_question", name="new_question")
     * @param Request $request
     * @return Response
     * @Template()
     */
    public function newQuestionAction(Request $request)
    {
        $action = $request->request->get('action');
        /** @var EntityManager $entityManager */
        $entityManager = $this->getDoctrine()->getManager();
        if ($action == 'new' || !$action) {
            $question = new Question();
            $newQuestionForm = $this->createForm(CreateNewQuestionType::class, $question);
            $newQuestionForm->handleRequest($request);
            if ($newQuestionForm->isValid()) {
                $testID = $request->request->get('testID');
                /** @var Test $test */
                $test = $entityManager->getRepository(Test::class)->find($testID);
                $question->setTest($test);
                $entityManager->persist($question);
                $entityManager->flush();
            }

            return $this->render('AppBundle:admin:question/new_question.html.twig', ['form' => $newQuestionForm->createView()]);

        } elseif ($action == 'delete') {
            $question = $entityManager->getRepository(Question::class)->find($request->request->get('questionID'));
            if ($question) {
                $entityManager->remove($question);
                $entityManager->flush();

                return new Response("true");
            } else {
                return new Response("false");
            }
        } elseif ($action == 'edit') {
            $question = $entityManager->getRepository(Question::class)->find($request->request->get('questionID'));
            $editQuestionForm = $this->createForm(CreateNewQuestionType::class, $question);
            $editQuestionForm->handleRequest($request);

            if ($editQuestionForm->isValid()) {
                $entityManager->persist($question);
                $entityManager->flush();
            }

            return $this->render('AppBundle:admin:question/new_question.html.twig', ['form' => $editQuestionForm->createView()]);
        }

        return new Response("");
    }

    /**
     * @Route("/get_all_questions")
     * @param Request $request
     * @return Response
     * @Template()
     */
    public function getAllQuestions(Request $request)
    {
        if (!$request->isXmlHttpRequest()) {
           return $this->redirect($this->generateUrl('index'));
        }

        $testID = $request->request->get('testID');
        /** @var Test $test */
        $test = $this->getDoctrine()->getManager()->getRepository(Test::class)->find($testID);

        return $this->render('AppBundle:admin:question/questions_list.html.twig', ['questions' => $test->getQuestions()->toArray()]);
    }
}