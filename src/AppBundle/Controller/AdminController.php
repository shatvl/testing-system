<?php
/**
 * Created by PhpStorm.
 * User: Vladislav
 * Date: 04.11.2016
 * Time: 12:33
 */

namespace AppBundle\Controller;

use DOMDocument;
use PhpOffice\PhpWord\IOFactory;
use PhpOffice\PhpWord\TemplateProcessor;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use ZipArchive;

class AdminController extends Controller {

    /**
     * @Route("/admin", name="index")
     * @Template()
     */
    public function indexAction() {

    }

}