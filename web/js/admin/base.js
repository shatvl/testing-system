$(function(){
    initTests();
    initCompletedTests();
});

function initCompletedTests() {
    $('#all-completed-tests').on("click", function(){
       waitingDialog.show();
        $.ajax({
            method: "POST",
            url: "/get_all_completed_tests",
            success: function(data) {
                waitingDialog.hide();
                $('.page-header').text('Completed Tests');
                $('.page--container').html(data);


            }
        })
    });
}

function initTests() {
    $('#all-tests-link').on("click", function(){
        waitingDialog.show();
        $.ajax({
            method: "POST",
            url: "/get_all_tests",
            success: function(data) {
                waitingDialog.hide();
                $('.page-header').text('Tests');
                $('.page--container').html(data);

                $('.question-panel').on('show.bs.collapse', function(e) {
                    var questionPanel = $(this);
                    if (!$(this).hasClass('table-loaded')) {
                        var progressBar = $(this).find('.progress');
                        progressBar.show();
                        var id = $(this).data('id');
                        $.ajax({
                            method: "POST",
                            url: "/get_all_questions",
                            data: {testID: id},
                            success: function (data) {
                                progressBar.hide();
                                var body = $('.panel-body-table-' + id).html(data);
                                questionPanel.addClass('table-loaded');
                            }
                        });
                    }
                });
            }
        });
        return false;
    });
}

function deleteTest(event, testID) {
    event.stopPropagation();
    $('#confirm-delete').modal('show');
    $('#confirm-delete').on('click', '.btn-ok', function(){
        waitingDialog.show();
        $.ajax({
            method: "POST",
            url: "/create_new_test",
            data: { action: "delete", testID: testID },
            success: function(data) {
                waitingDialog.hide();
                if(data == "true") {
                    $('#confirm-delete').modal('hide');
                    $('#all-tests-link').trigger("click");
                }
            }
        })
    });
}

function editTest(event, testID) {
    event.stopPropagation();
    waitingDialog.show();
    $.ajax({
        method: "POST",
        url: "/create_new_test",
        data: { action: "edit", testID: testID },
        success: function (data) {
            waitingDialog.hide();
            $('.modals--container').html(data);
            $('#create-new-test-modal').modal('show');
            $('form[name="create_new_test"]').on("submit", function(e) {
                e.preventDefault();
                var formData = new FormData(this);
                formData.append('action', 'edit');
                formData.append('testID', testID);
                waitingDialog.show();
                $.ajax({
                    method: "POST",
                    url: "/create_new_test",
                    data: formData,
                    processData: false,
                    contentType: false,
                    success: function(data) {
                        waitingDialog.hide();
                        $('#create-new-test-modal').modal('hide');
                        $('#all-tests-link').trigger("click");
                    }
                });
            });
        }
    })
}

function createNewTest() {
    waitingDialog.show();
    $.ajax({
        method: "POST",
        url: "/create_new_test",
        data: { action: "new" },
        success: function (data){
            waitingDialog.hide();
            $('.modals--container').html(data);
            $('#create-new-test-modal').modal('show');
            $('form[name="create_new_test"]').on("submit", function(e) {
                waitingDialog.show();
                e.preventDefault();
                $.ajax({
                    method: "POST",
                    url: "/create_new_test",
                    data: new FormData(this),
                    processData: false,
                    contentType: false,
                    success: function(data) {
                        waitingDialog.hide();
                        $('#create-new-test-modal').modal('hide');
                        $('#all-tests-link').trigger("click");
                    }
                });
            });
        }
    });
}

function editQuestion(event, questionID) {
    event.stopPropagation();
    waitingDialog.show();
    $.ajax({
        method: "POST",
        url: "/create_new_question",
        data: { action: "edit", questionID: questionID },
        success: function (data) {
            waitingDialog.hide();
            $('.modals--container').html(data);
            tinymce.init({ selector:'textarea',
                menubar: false,
                plugins: [
                    'advlist autolink lists link image charmap print preview anchor',
                    'searchreplace visualblocks code fullscreen',
                    'insertdatetime media table contextmenu paste code'
                ],
                toolbar: 'undo redo | insert | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
                content_css: '//www.tinymce.com/css/codepen.min.css',
                paste_data_images: true
            });
            $('#create-new-question-modal').modal('show');
            $('#create-new-question-modal').on('hidden.bs.modal', function () {
                tinymce.remove();
            });
            $('form[name="create_new_question"]').on("submit", function(e) {
                waitingDialog.show();
                e.preventDefault();
                var formData = new FormData(this);
                formData.append('action', 'edit');
                formData.append('questionID', questionID);
                $.ajax({
                    method: "POST",
                    url: "/create_new_question",
                    data: formData,
                    processData: false,
                    contentType: false,
                    success: function(data) {
                        waitingDialog.hide();
                        $('#create-new-question-modal').modal('hide');
                        $('#all-tests-link').trigger("click");
                    }
                });
            });
        }
    });
}

function deleteQuestion(event, questionID) {
    event.stopPropagation();
    $('#confirm-delete').modal('show');
    $('#confirm-delete').on('click', '.btn-ok', function(){
        waitingDialog.show();
        $.ajax({
            method: "POST",
            url: "/create_new_question",
            data: { action: "delete", questionID: questionID },
            success: function(data) {
                waitingDialog.hide();
                if(data == "true") {
                    $('#confirm-delete').modal('hide');
                    $('#all-tests-link').trigger("click");
                }
            }
        })
    });
}

function createNewQuestion(testID) {
    waitingDialog.show();
    $.ajax({
        method: "POST",
        url: "/create_new_question",
        data: { new: true },
        success: function (data) {
            waitingDialog.hide();
            $('.modals--container').html(data);
            tinymce.init({ selector:'textarea',
                menubar: false,
                plugins: [
                    'advlist autolink lists link image charmap print preview anchor',
                    'searchreplace visualblocks code fullscreen',
                    'insertdatetime media table contextmenu paste code'
                ],
                toolbar: 'undo redo | insert | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
                content_css: '//www.tinymce.com/css/codepen.min.css',
                paste_data_images: true
            });
            $('#create-new-question-modal').on('hidden.bs.modal', function () {
                tinymce.remove();
            });
            $('#create-new-question-modal').modal('show');
            $('form[name="create_new_question"]').on("submit", function(e) {
                waitingDialog.show();
                var formData = new FormData(this);
                formData.append('testID', testID);
                $.ajax({
                    method: "POST",
                    url: "/create_new_question",
                    data: formData,
                    processData: false,
                    contentType: false,
                    success: function(data) {
                        waitingDialog.hide();
                        $('#create-new-question-modal').modal('hide');
                        $('#all-tests-link').trigger("click");
                    }
                })
            })
        }
    })
}

function addNewAnswer() {
    var answersList = $('#answers-fields-list');
    var newWidget = answersList.attr('data-prototype');
    var answerCount = $('#answers-fields-list li').length;
    newWidget = newWidget.replace(/__name__/g, answerCount);
    answerCount++;
    var newLi = $('<li></li>').html(newWidget);
    newLi.appendTo(answersList);
    tinymce.init({ selector:'textarea',
        menubar: false,
        plugins: [
            'advlist autolink lists link image charmap print preview anchor',
            'searchreplace visualblocks code fullscreen',
            'insertdatetime media table contextmenu paste code'
        ],
        toolbar: 'undo redo | insert | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
        content_css: '//www.tinymce.com/css/codepen.min.css',
        paste_data_images: true
    });
}

var waitingDialog = waitingDialog || (function ($) {
        'use strict';

        // Creating modal dialog's DOM
        var $dialog = $(
            '<div class="modal fade" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-hidden="true" style="padding-top: 150px; margin-top: -50px; overflow-y:visible;">' +
            '<div class="modal-dialog modal-m">' +
            '<div class="modal-content">' +
            '<div class="modal-header"><h3 style="margin:0;"></h3></div>' +
            '<div class="modal-body">' +
            '<div class="progress progress-striped active" style="margin-bottom:0;"><div class="progress-bar" style="width: 100%"></div></div>' +
            '</div>' +
            '</div></div></div>');

        return {
            /**
             * Opens our dialog
             * @param message Custom message
             * @param options Custom options:
             * 				  options.dialogSize - bootstrap postfix for dialog size, e.g. "sm", "m";
             * 				  options.progressType - bootstrap postfix for progress bar type, e.g. "success", "warning".
             */
            show: function (message, options) {
                // Assigning defaults
                if (typeof options === 'undefined') {
                    options = {};
                }
                if (typeof message === 'undefined') {
                    message = 'Loading';
                }
                var settings = $.extend({
                    dialogSize: 'm',
                    progressType: '',
                    onHide: null // This callback runs after the dialog was hidden
                }, options);

                // Configuring dialog
                $dialog.find('.modal-dialog').attr('class', 'modal-dialog').addClass('modal-' + settings.dialogSize);
                $dialog.find('.progress-bar').attr('class', 'progress-bar');
                if (settings.progressType) {
                    $dialog.find('.progress-bar').addClass('progress-bar-' + settings.progressType);
                }
                $dialog.find('h3').text(message);
                // Adding callbacks
                if (typeof settings.onHide === 'function') {
                    $dialog.off('hidden.bs.modal').on('hidden.bs.modal', function (e) {
                        settings.onHide.call($dialog);
                    });
                }
                // Opening dialog
                $dialog.modal();
            },
            /**
             * Closes dialog
             */
            hide: function () {
                $dialog.modal('hide');
            }
        };

    })(jQuery);