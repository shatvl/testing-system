$(function () {
   $('[data-toggle="tooltip"]').tooltip();

   $(document).on("click", ".test-start-btn", function() {
      $('.tests-container').hide();
      $('.cssload-thecube').show();
      var testId = $(this).data('id');
      $.ajax({
          method: "POST",
          url: "/test/" + testId,
          success: function (data) {
              data = JSON.parse(data);
              $('#quiz').quiz({
                  //resultsScreen: '#results-screen',
                  //counter: false,
                  //homeButton: '#custom-home',
                  counterFormat: 'Question %current of %total',
                  questions: data,
                  finishCallback: function(data, data1) {
                      $("#quiz-restart-btn").text("Choose another test");
                      $("#quiz-restart-btn").attr("id", "quiz-end");
                      $("#quiz-end").click(function (event) {
                          window.location = "/";
                      });
                      $.ajax({
                          method: "POST",
                          url: "/test-result/" + testId,
                          data: {correct: data, all: data1},
                          success: function(data) {
                          }
                      })
                  }
              });
              $('.cssload-thecube').hide();
              $('#quiz-header').show();
              $('#quiz-start-screen').show();
          }
      });
   });
});